using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;

public class AdvancementCard : MonoBehaviour
{

	[ShowInInspector]
	public Dictionary<string, CardField> Fields = new Dictionary<string, CardField>();


	void Start()
	{
		var foundFields = GetComponentsInChildren<CardField>();
		foreach(var f in foundFields)
		{
				Fields[f.gameObject.name] = f;
		}
	}

	public void DisplayAdvancement(Dictionary<string, string> advancement, string version)
	{
		Fields["Name"].SetData(advancement["Name"]);
		Fields["Type"].SetData(advancement["Type"].ToUpper()); //More to do
		Fields["Cost"].SetData(advancement["Cost"][0].ToString()); //More to do
		Fields["Text"].SetData(advancement["Text"]);
		Fields["Version"].SetData(version);
	}
}
