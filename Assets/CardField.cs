using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CardField : MonoBehaviour
{
	public abstract void SetData(string data);
}
