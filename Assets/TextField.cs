using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextField : CardField
{
	override public void SetData(string data)
	{
		GetComponent<TextMeshProUGUI>().text = data;
	}
}
