using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleSheetsForUnity;
using Sirenix.OdinInspector;
using Newtonsoft.Json;

public class CardGenerator : MonoBehaviour
{
	public string tableName = "Advancements";
	public AdvancementCard advancementCard;


	void Start()
	{
		//Drive.GetAllTables();
		Drive.GetTable("Advancements");
	}

	List<Dictionary<string, string>> advancements = new List<Dictionary<string, string>>();

	float countdown = 0;
	void Update()
	{
		countdown -= Time.deltaTime;
		if (countdown < 0)
		{
			countdown = 3;
			if(advancements != null && advancements.Count > 0)
			{
				var advancement = advancements[Random.Range(0, advancements.Count)];
				advancementCard.DisplayAdvancement(advancement, "TEST");
				var filename = "sta advancement " + advancement["Name"] + ".png";
				ScreenCapture.CaptureScreenshot(filename);
			}
		}

		
	}


	private void OnEnable()
	{
		// Suscribe for catching cloud responses.
		Drive.responseCallback += HandleDriveResponse;
	}

	private void OnDisable()
	{
		// Remove listeners.
		Drive.responseCallback -= HandleDriveResponse;
	}


	void HandleAdvancementsList(List<Dictionary<string, string>> advancements)
	{
		string logMsg = "<color=yellow>" + advancements.Count.ToString() + " advancements retrieved and parsed:</color>";
		for (int i = 0; i < advancements.Count; i++)
		{
			var advancement = advancements[i];
			logMsg += "\n" +
				"<color=red>Name: " + advancement["Name"] + "</color>\n";
			foreach (var key in advancement.Keys)
			{
				if (key != "Name")
					logMsg += $"{key}: {advancement[key]}\n";
			}
		}
		Debug.Log(logMsg);
		this.advancements = advancements;
	}
	
	public Drive.DataContainer[] tables;

	public void HandleDriveResponse(Drive.DataContainer dataContainer)
	{
		Debug.Log("HandleDriveResponse");
		Debug.Log(dataContainer.msg);
		Debug.Log(dataContainer.QueryType);
		Debug.Log(dataContainer.payload);

		// First check the type of answer.
		if (dataContainer.QueryType == Drive.QueryType.getTable)
		{
			string rawJSon = dataContainer.payload;
			Debug.Log(rawJSon);

			// Check if the type is correct.
			if (string.Compare(dataContainer.objType, tableName) == 0)
			{
				// Parse from json to the desired object type.
				var advancements = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(rawJSon);
				HandleAdvancementsList(advancements);
			}
			else
			{
				Debug.LogError($"Str test failed!!!! {dataContainer.objType} {tableName}");
			}
		}

		//GetAllTables
		if (dataContainer.QueryType == Drive.QueryType.getAllTables)
		{
			string rawJSon = dataContainer.payload;

			// The response for this query is a json list of objects that hold tow fields:
			// * objType: the table name (we use for identifying the type).
			// * payload: the contents of the table in json format.
			tables = JsonHelper.ArrayFromJson<Drive.DataContainer>(rawJSon);

			// Once we get the list of tables, we could use the objTypes to know the type and convert json to specific objects.
			// On this example, we will just dump all content to the console, sorted by table name.
			string logMsg = "<color=yellow>All data tables retrieved from the cloud.\n</color>";
			for (int i = 0; i < tables.Length; i++)
			{
				logMsg += "\n<color=blue>Table Name: " + tables[i].objType + "</color>\n" + tables[i].payload + "\n";
			}
			Debug.Log(logMsg);
		}
	}
}